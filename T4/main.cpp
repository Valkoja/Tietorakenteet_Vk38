#include "main.h"

// Write a program that reads a sequence of integers of increasing size and prints  the integers in decreasing order of size.
// Input terminates as soon as an integer  that does not exceed its predecessor is read. The integers are then printed in  decreasing order.

int main()
{
    bool read = true;
    int input = 0;
    int output = 0;
    Stack numbers;

    std::cout << "Anna ensimmainen numero" << std::endl;
    std::cin >> input;
    numbers.push(input);

    while (read)
    {
        std::cout << "Anna seuraava numero" << std::endl;
        std::cin >> input;
        numbers.top(output);

        if (input > output)
        {
            numbers.push(input);
        }
        else
        {
            read = false;
        }
    }

    while (!numbers.empty())
    {
        numbers.top(output);
        numbers.pop();
        std::cout << output << " ";
    }

    std::cout << std::endl;

    return 0;
}
