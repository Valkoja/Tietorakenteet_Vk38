#pragma once

typedef int Stack_entry;

class Stack
{
    public:
       Stack();
       bool empty() const;
       Error_code pop();
       Error_code top(Stack_entry &item) const;
       Error_code push(const Stack_entry &item);

    protected:
       int count;
       static const int maxstack = 1024;
       Stack_entry entry[maxstack];
};
