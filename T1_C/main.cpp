#include "main.h"

// Jatkotehtävä 2: muuta jatkotehtävän ratkaisua siten, että ohjelma tutkii onko syötetty tekstinpätkä palindromi.
// Palindromi on sellainen tekstinpätkä, jonka sisältö on sama luettiinpa sen merkit alusta loppuun tai lopusta alkuun päin.
// Esimerkki  palindromista: "saippuakauppias". Tarkastus on tehtävä pinon avulla.

int main()
{
    bool palindrome = true;
    std::string input = "";
    std::stack<char> temp;
    std::stack<char> letters;
    std::stack<char> swapped;

    std::cout << "Anna rivi" << std::endl;
    getline(std::cin, input);

    for (int i = 0; i < input.length(); i++)
    {
        temp.push(tolower(input[i]));
        letters.push(tolower(input[i]));
    }

    while (!temp.empty())
    {
        swapped.push(temp.top());
        temp.pop();
    }

    while (!letters.empty())
    {
        if (letters.top() != swapped.top())
        {
            palindrome = false;
            break;
        }

        letters.pop();
        swapped.pop();
    }

    if (palindrome == false)
    {
        std::cout << "Rivi ei ole palindromi" << std::endl;
    }
    else
    {
        std::cout << "Rivi on palindromi" << std::endl;
    }

    return 0;
}
