#include "main.h"

// Käännä, linkkaa ja kokeile luentokalvoissa olevan "First Example: Reversing a List" -esimerkin toimintaa.

int main()
{
   int n;
   double item;
   std::stack<double> numbers;

   std::cout << " Type in an integer n followed by n decimal numbers." << std::endl << " The numbers will be printed in reverse order." << std::endl;
   std::cin >> n;

   for (int i = 0; i < n; i++)
   {
      std::cin >> item;
      numbers.push(item);
   }

   std::cout << std::endl << std::endl;

   while (!numbers.empty())
   {
      std::cout << numbers.top() << " ";
      numbers.pop();
   }

   std::cout << std::endl;

   return 0;
}
