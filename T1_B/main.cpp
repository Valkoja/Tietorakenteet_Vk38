#include "main.h"

// Jatkotehtävä: muuta esimerkkikoodin toimintaa siten, että ohjelma lukeekin rivin tekstiä ja tulostaa sen merkit käänteisessä järjestyksessä.

int main()
{
    std::string input = "";
    std::stack<char> letters;

    std::cout << "Anna rivi" << std::endl;
    getline(std::cin, input);

    for (int i = 0; i < input.length(); i++)
    {
        letters.push(input[i]);
    }

    std::cout << std::endl;

    while (!letters.empty())
    {
        std::cout << letters.top() << " ";
        letters.pop();
    }

    std::cout << std::endl;

    return 0;
}
