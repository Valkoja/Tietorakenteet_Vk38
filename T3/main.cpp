#include "main.h"

// Write a program that makes use of a stack to read in a single line of text and write out the characters in the line in reverse order.

int main()
{
    char output;
    Stack letters;
    std::string input = "";

    std::cout << "Anna rivi" << std::endl;
    getline(std::cin, input);

    for (int i = 0; i < input.length(); i++)
    {
        letters.push(input[i]);
    }

    std::cout << std::endl;

    while (!letters.empty())
    {
        letters.top(output);
        letters.pop();
        std::cout << output << " ";
    }

    std::cout << std::endl;

    return 0;
}
