#pragma once

#include <iostream>
#include <stdlib.h>
#include <time.h>

enum Error_code {success, fail, utility_range_error, underflow, overflow, fatal, not_present, duplicate_error, entry_inserted, entry_found, internal_error};

#include "stack.h"
#include "extended_stack.h"
