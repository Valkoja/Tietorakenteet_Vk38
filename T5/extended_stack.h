#pragma once

class Extended_stack: public Stack
{
    public:
        Extended_stack();
        void clear();
        bool full() const;
        int size() const;
};
