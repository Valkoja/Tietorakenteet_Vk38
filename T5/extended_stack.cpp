#include "main.h"

Extended_stack::Extended_stack() : Stack()
{
    // Kutsutaan parentin rakentajaa
}

// Reset the stack to be empty.
void Extended_stack::clear()
{
    count = 0;
}

// If the stack is full, return true; else return false.
bool Extended_stack::full() const
{
    bool isFull = false;

    if (count == maxstack)
        isFull = true;

    return isFull;
}

 // Return the number of entries in the stack.
int Extended_stack::size() const
{
    return count;
}
