#include "main.h"

// Assume the following definition file for a contiguous implementation of an extended stack data structure.

//     class Extended_stack {
//     public:
//         Extended_stack( );
//         Error_code pop( );
//         Error_code push(const Stack_entry &item);
//         Error_code top(Stack_entry &item) const;
//         bool empty( ) const;
//         void clear( ); // Reset the stack to be empty.
//         bool full( ) const ; // If the stack is full, return true; else return false.
//         int size( ) const; // Return the number of entries in the stack.
//
//     private:
//         int count;
//         Stack_entry entry[maxstack];
//     };

// Write code for the following methods. [Use the private data members in your code.]
// (a) clear (b) full (c) size
// Write also test driver program for these methods and test with it that your methods work properly.

int main()
{
    srand(time(NULL));
    Extended_stack numbers;

    int input;

    while (numbers.full() == false)
    {
        input = rand() % 100;

        if (input == 42)
        {
            break;
        }

        numbers.push(input);
    }

    std::cout << "Stackiin paatyi " << numbers.size() << " numeroa" << std::endl;

    numbers.clear();

    std::cout << "Ja clearin jalkeen jaljella on " << numbers.size() << " numeroa" << std::endl;

    return 0;
}
